import { Component, OnInit } from '@angular/core';
import { freeApiService } from '../services/freeapi.service';

import { Comments } from '../classes/comments';
@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(private _freeApiService: freeApiService ) {
  }

  lstcomments:Comments[];

  ngOnInit() {
    this._freeApiService.getcomments()
    .subscribe
    (
      data=>
      {
        this.lstcomments = data.data;
      }
    );
  }





}
